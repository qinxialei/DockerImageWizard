#!/bin/bash
declare -A project_info
project_info[name]=""              #项目名称
project_info[path]=""              #项目路径
project_info[exec_command]=""      #项目运行方式
project_info[type]=0               #项目类型  1:服务端  2:客户端   3:位置服务
project_info[service_ip]=""        #位置服务IP
project_info[service_port]=0       #位置服务开放端口，仅在打包客户端项目时使用
project_info[exposure_port]=0      #对外提供的端口号，仅在打包服务端和位置服务的时候使用
thirdparty_path=()                 #三方库具体路径，仅在打包客户端项目时使用
file_path=()                       #文件具体路径

whip_title="Acosail容器构建向导"
get_type() {
    type=$(whiptail --title "$whip_title" --radiolist \
    "项目属于下面哪种类型(上下键选择，空格选中，回车确认)" \
     10 60 3\
    "1" "服务端程序               " ON \
    "2" "客户端程序               " OFF \
    "3" "位置服务程序             " OFF 3>&1 1>&2 2>&3)
    exitstatus=$?
    if [ $exitstatus -eq 0 ];then
        return $type
    else
        exit -1
    fi
}
get_files() {
    files=()
    tmp_file_count=0
    file_path_count=0
    choices=""
    for tmp_file in `find ${project_info[path]} -type f -not -name "*.h" \
                     -not -name "*.c" -not -name "*.cpp" -not -name "*.o" \
                     -printf "%P\n"`
    do
        files[tmp_file_count]=$tmp_file
        choices+="$tmp_file_count $tmp_file OFF "
        ((tmp_file_count+=1))
    done
    file_select=$(whiptail --title "$whip_title" \
    --checklist "选择打包文件(方向键选择，空格选中，回车确认):" \
    10 60 3 $choices 3>&1 1>&2 2>&3)
    exitstatus=$?
    if [ $exitstatus -eq 0 ];then
        if [ ${#file_select[@]} -eq 0 ];then
            return 1
        fi
        IFS=" " read -ra file_array <<< "$file_select"
        for select_file_count in "${file_array[@]}"
        do
            select_file_count="${select_file_count#\"}"
            select_file_count="${select_file_count%\"}"
            file_path[${file_path_count}]=${files[$select_file_count]}
            ((file_path_count += 1))
        done
        return 0
    else
        exit -1
    fi
}
reget_files() {
    $(whiptail --title "$whip_title" --yesno \
    "你刚刚没有选择任何文件，是否需要重新选择？" 10 60 3 \
    3>&1 1>&2 2>&3)
    need_re_get=$?
    if [ $need_re_get -eq 0 ];then
        get_files
    fi
    return 0
}
get_thirdparty() {
    $(whiptail --title "$whip_title" --yesno \
    "项目是否使用了三方库（包含VSOA）？" 10 60 3 \
    3>&1 1>&2 2>&3)
    has_thirdparty=$?
    if [ $has_thirdparty -ne 0 ];then
        return 0
    fi
    # 存在三方库
    need_input=0
    thirdparty_count=0
    while [ $need_input -eq 0 ];
    do
        tmp_thirdparty_path=$(whiptail --title "$whip_title" --inputbox \
        "使用的三方库路径，精确到.so" 10 60 --ok-button "添加" --cancel-button "结束" \
        3>&1 1>&2 2>&3)
        exitstatus=$?
        if [ $exitstatus -eq 0 ];then
            # 存在多个三方库
            thirdparty_path[$thirdparty_count]=$tmp_thirdparty_path
            ((thirdparty_count += 1))
        else
            if [ ! -z $tmp_thirdparty_path ];then
                thirdparty_path[$thirdparty_count]=$tmp_thirdparty_path
                ((thirdparty_count += 1))
            fi
            need_input=1
        fi
    done
    return 0
}
get_ip_and_port() {
    ip_port=$(whiptail --title "$whip_title" --inputbox \
    "请输入位置服务的IP和端口，中间用:隔开,若不需要则跳过" 10 60 "127.0.0.1:3000" \
    --cancel-button="跳过" 3>&1 1>&2 2>&3)
    exitstatus=$?
    if [ $exitstatus -eq 0 ] && [ ! -z $ip_port ];then
        #array=(${ip_port//:/})
        project_info[service_port]=${ip_port##*:}
        project_info[service_ip]=${ip_port%%:*}
    fi
    return 0
}
get_exec() {
    tmp_exec=$(whiptail --title "$whip_title" --inputbox \
    "请输入项目的运行命令" 10 60 3>&1 1>&2 2>&3)
    exitstatus=$? 
    if [ $exitstatus -eq 0 ];then
        project_info[exec_command]="$tmp_exec"
    fi
    return 0;
}
get_exposure_port() {
    tmp_port=$(whiptail --title "$whip_title" --inputbox \
    "请输入对外提供的端口号" 10 60 "3000" 3>&1 1>&2 2>&3)
    exitstatus=$? 
    if [ $exitstatus -eq 0 ];then
        project_info[exposure_port]="$tmp_port"
    fi
    return 0;
}
create_tar_file() {
    if [ ! -d "/etc/vsoa/" ];then
        mkdir -p /etc/vsoa/
    fi
    mkdir -p /etc/vsoa/${project_info[name]}/{libs,bin}
    tar_file_path="/etc/vsoa/${project_info[name]}"
    for tmp_bin_file in "${file_path[@]}"
    do
        tmp_target_file_path=${tar_file_path}"/bin/"${tmp_bin_file}
        if [ ! -d `dirname ${tmp_target_file_path}` ];then
            mkdir -p `dirname ${tmp_target_file_path}`
        fi
        cp ${project_info[path]}"/"${tmp_bin_file} $tmp_target_file_path
    done

    for tmp_so_file in ${thirdparty_path[@]}
    do
        tmp_target_file_path=${tar_file_path}"/libs/"`basename ${tmp_so_file}`
        if [ ! -d `dirname ${tmp_target_file_path}` ];then
            mkdir -p `dirname ${tmp_target_file_path}`
        fi
        cp ${tmp_so_file} $tmp_target_file_path
    done

    if [ ${project_info[type]} -eq 2 ] && [ ${project_info[service_port]} -ne 0 ] \
        && [ ! -z "${project_info[service_ip]}" ];then
        echo "${project_info[service_ip]} ${project_info[service_port]}" >> /etc/vsoa/vsoa.pos
    fi

    old_pwd=`pwd`
    cd /etc/vsoa
    tar -zcvf project.tar.gz ${project_info[name]}
    cd ${old_pwd}
}
create_docker_file() {
    if [ ! -d "/etc/vsoa/" ];then
        mkdir -p /etc/vsoa/
    fi
    touch /etc/vsoa/Dockerfile
    #echo "FROM scratch" >> /etc/vsoa/Dockerfile
    #echo "ADD rootfs.tar.xz /" >> /etc/vsoa/Dockerfile
    echo "FROM busybox:glibc" >> /etc/vsoa/Dockerfile
    echo "ADD project.tar.gz /app" >> /etc/vsoa/Dockerfile
    echo "ADD vsoa.pos /etc" >> /etc/vsoa/Dockerfile
    echo "ENV LD_LIBRARY_PATH \$LD_LIBRARY_PATH:/app/${project_info[name]}/libs" >> /etc/vsoa/Dockerfile
    if [ ${project_info[type]} -eq 1 ] || [ ${project_info[type]} -eq 3 ];then
        echo "EXPOSE ${project_info[exposure_port]}" >> /etc/vsoa/Dockerfile
    fi
    echo "CMD [\"/app/${project_info[name]}/bin/${project_info[exec_command]}\"]" \
         >> /etc/vsoa/Dockerfile
}
create_docker_image() {
    client_image_name="client:latest"
    service_image_name="service:latest"
    position_image_name="position:latest"
    client_image_save_name="docker_client_rpc.tar"
    service_image_save_name="docker_service_rpc.tar"
    position_image_save_name="docker_position_rpc.tar"
    create_image_name=""
    save_image_name=""
    if [ ${project_info[type]} -eq 1 ];then
        create_image_name=$service_image_name
        save_image_name=$service_image_save_name
    elif [ ${project_info[type]} -eq 2 ];then
        create_image_name=$client_image_name
        save_image_name=$client_image_save_name
    else
        create_image_name=$position_image_name
        save_image_name=$position_image_save_name
    fi

    # 创建image
    cd /etc/vsoa/
    docker build -t ${create_image_name} .

    # 导出image
    docker save -o ${save_image_name} ${create_image_name}

    # 删除mage
    docker image rm ${create_image_name}
}
clean() {
    rm /etc/vsoa/Dockerfile
    rm -rf /etc/vsoa/${project_info[name]}
    rm -rf /etc/vsoa/project.tar.gz
    rm -rf /etc/vsoa/root.tar.xz
    rm -rf /etc/vsoa/etc
}
main() {
    if [ $# -eq 0 ] || [ ! -d $1 ];then
        # 参数不对，退出
        echo "The only argument is needed a directory of the project"
        exit -1
    fi
    if [ 0 -ne `id -u` ];then
        echo "Your should run this script whit root access"
        exit -1
    fi
    # Get the project name
    project_info[path]="$1"
    project_info[name]=`basename $1`

    # Get the project type
    get_type
    type=$?
    if [ $? -eq -1 ];then
        # 用户选择了退出
        exit -1
    fi
    project_info[type]=$type

    # Get the needed file of this project
    get_files
    get_files_result=$?
    if [ $get_files_result -eq -1 ];then
        # 用户选择了退出
        return  -1
    elif [ $get_files_result -eq 1 ];then
        # 用户选择了确定，但是没有选择任何需要包含的文件
        # 再次询问一次用户
        reget_files
    fi

    # Get the thirdparty if this project needed
    get_thirdparty

    # Get the service ip and port
    # if is client ,we need the position`s ip and port info
    # but if has ecsm, the position is not needed,so it`s ip 
    # and port can be null
    # if is service, we need the service`s port used for exposure
    # if is position, we need the position`s port used for exposure
    if [ $type -eq 2 ];then
        get_ip_and_port
    else
        get_exposure_port
    fi

    # Get the exec of this project
    get_exec

    #create_image &
    #job_pid=$!

    # create the project.tar.gz
    create_tar_file

    # create DockerFile
    create_docker_file

    # create Docker image
    create_docker_image

    #whiptail --title "${whip_title}" --msgbox "docker镜像创建成功，在/etc/vsoa/" 10 30
    echo "docker image create success! the image file is saved in /etc/vsoa/"
    # clean
    clean
}
main $@
exit 0

